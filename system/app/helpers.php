<?php

use Carbon\Carbon;

function eng_to_ind_months()
{
  return array(
    "January" => "Januari",
    "February" => "Februari",
    "March" => "Maret",
    "April" => "April",
    "May" => "Mei",
    "June" => "Juni",
    "July" => "Juli",
    "August" => "Agustus",
    "September" => "September",
    "October" => "Oktober",
    "November" => "November",
    "December" => "Desember"
  );
}

function carbon_date($date)
{
  return Carbon::createFromFormat('Y-m-d', $date);
}

function indo_date($date)
{
  if (is_string($date)) {
    $date = Carbon::createFromFormat('Y-m-d', $date);
  }
  $str =  $date->format('j F Y');
  $eng_to_ind_months = eng_to_ind_months();
  foreach ($eng_to_ind_months as $eng => $ind) {
    $str = str_replace($eng, $ind, $str);
  }
  return $str;
}

function indo_datetime($date)
{
  if (is_string($date)) {
    $date = Carbon::createFromFormat('Y-m-d H:i:s', $date);
  }
  $str =  $date->format('j F Y, h:i A');
  $eng_to_ind_months = eng_to_ind_months();
  foreach ($eng_to_ind_months as $eng => $ind) {
    $str = str_replace($eng, $ind, $str);
  }
  return $str;
}

function indo_currency($number)
{
  return "Rp " . number_format($number, 0, ",", ".");
}

function custom_elixir($file)
{
    static $manifest = null;

    if (is_null($manifest)) {
      $manifest = json_decode(file_get_contents(public_path('..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'build'.DIRECTORY_SEPARATOR.'rev-manifest.json')), true);
    }

    if (isset($manifest[$file])) {
      return url('/build/'.$manifest[$file]);
    }

    throw new InvalidArgumentException("File {$file} not defined in asset manifest.");
}