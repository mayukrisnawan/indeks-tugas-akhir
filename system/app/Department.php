<?php

namespace IndeksTugasAkhir;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
  protected $fillable = ['name'];

  public function final_tasks()
  {
    return $this->hasMany('IndeksTugasAkhir\FinalTask');
  }

  public static function options()
  {
    $departments = Department::all();
    $options = [];
    $departments->sortBy('name')->each(function($department) use (&$options){
      $options[$department->id] = $department->name;
    });
    return $options;
  }
}
