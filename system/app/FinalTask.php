<?php

namespace IndeksTugasAkhir;

use DB;
use File;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
use League\Csv\Reader;

class FinalTask extends Model
{
  protected $fillable = ['title', 'student_name', 'nim', 'department_id', 'date', 'rate', 'score', 'comment'];

  public function department()
  {
    return $this->belongsTo('IndeksTugasAkhir\Department');
  }

  public static function storeCsv($file) {
    $filename = time() . ".csv";
    $file->move('.', $filename);
    $reader = Reader::createFromPath($filename);

    $final_tasks = collect([]);
    $is_header_found = false;
    foreach ($reader as $index => $row) {
      if (strtoupper($row[0]) == "NO") {
        $is_header_found = true;
        continue;
      }

      if ($is_header_found) {
        $final_task = array(
          'student_name' => $row[1],
          'nim' => $row[2],
          'department' => $row[3],
          'date' => FinalTask::parseDate($row[4]),
          'rate' => $row[6],
          'comment' => $row[7],
          'title' => $row[8],
          'score' => $row[9],
        );
        $final_tasks->push($final_task);
      }
    }
    File::delete($filename);
    DB::transaction(function() use (&$final_tasks){
      $final_tasks->each(function($attrs){
        $final_task = FinalTask::where('nim', $attrs['nim'])->first();
        $department = Department::where('name', $attrs['department'])->first();
        if ($department === null) {
          $department = new Department(['name' => $attrs['department']]);
          $department->save();
        }
        $attrs['department_id'] = $department->id;

        if (strlen($attrs['rate'] == 0)) unset($attrs['rate']);
        if ($final_task === null) {
          $final_task = new FinalTask($attrs);
          $final_task->save();
        } else {
          $final_task->update($attrs);
        }
      });
    });
  }

  public static function parseDate($dateStr) {
    $matches = array();
    $pattern1 = '/(\d{1,2}) ([a-zA-Z]+) (\d{4})/';
    $pattern2 = '/(\d{1,2})-([a-zA-Z]+)-(\d{2})/';
    $months = [
      'januari' => 1,
      'februari' => 2,
      'maret' => 3,
      'april' => 4,
      'mei' => 5,
      'juni' => 6,
      'juli' => 7,
      'agustus' => 8,
      'september' => 9,
      'oktober' => 10,
      'november' => 11,
      'desember' => 12,
    ];
    $short_months = [
      'jan' => 1,
      'feb' => 2,
      'mar' => 3,
      'apr' => 4,
      'may' => 5,
      'jun' => 6,
      'jul' => 7,
      'aug' => 8,
      'sep' => 9,
      'oct' => 10,
      'nov' => 11,
      'dec' => 12
    ];
    preg_match($pattern1, $dateStr, $matches);
    if (count($matches) == 4) {
      $year = intval($matches[3]);
      $month_name = strtolower($matches[2]);
      $month = $months[$month_name];
      $date = intval($matches[1]);
    } else {
      preg_match($pattern2, $dateStr, $matches);
      $year = intval(strlen($matches[3]) == '2' ? ('20' . $matches[3]) : $matches[3]);
      $month_name = strtolower($matches[2]);
      $month = $short_months[$month_name];
      $date = intval($matches[1]);
    }
    return Carbon::createFromDate($year, $month, $date);
  }

  public static function scoreOptions() {
    return [
      'A' => 'A',
      'B' => 'B',
      'C' => 'C',
      'D' => 'D',
      'E' => 'E'
    ];
  }
}
