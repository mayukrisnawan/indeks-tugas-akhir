<?php

namespace IndeksTugasAkhir\Providers;

use Illuminate\Support\ServiceProvider;
use IndeksTugasAkhir\Subject;
use IndeksTugasAkhir\Meeting;
use IndeksTugasAkhir\Subtopic;
use IndeksTugasAkhir\Reference;
use IndeksTugasAkhir\subjectAuditor;
use IndeksTugasAkhir\subjectAuditorLecturer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
