<?php

namespace IndeksTugasAkhir\Http\Requests;

use IndeksTugasAkhir\Http\Requests\Request;

class CreateFinalTaskRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'student_name' => 'required',
            'nim' => 'required|max:100|unique:final_tasks,nim',
            'title' => 'required',
            'department_id' => 'required|exists:departments,id',
        ];
    }
}
