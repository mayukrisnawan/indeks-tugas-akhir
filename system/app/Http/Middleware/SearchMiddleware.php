<?php

namespace IndeksTugasAkhir\Http\Middleware;

use Closure;

class SearchMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $search = false;
        if ($request->has('type') && $request->has('query')) {
            $search = [
                'type' => $request->get('type'),
                'query' => $request->get('query')
            ];
        }
        $request->merge(compact('search'));
        return $next($request);
    }
}
