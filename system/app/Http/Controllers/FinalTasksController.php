<?php

namespace IndeksTugasAkhir\Http\Controllers;

use Illuminate\Http\Request;
use IndeksTugasAkhir\Http\Requests;
use IndeksTugasAkhir\Http\Controllers\Controller;

use Carbon\Carbon;
use IndeksTugasAkhir\Department;
use IndeksTugasAkhir\FinalTask;
use IndeksTugasAkhir\Http\Requests\CreateFinalTaskRequest;
use IndeksTugasAkhir\Http\Requests\UpdateFinalTaskRequest;

class FinalTasksController extends Controller
{

    public function index(Request $request)
    {
        $search = $request->get('search');
        $final_tasks = FinalTask::orderBy($search['type'] ? $search['type'] : 'created_at', $search['type'] ? 'asc' : 'desc');
        if ($search) {
            $final_tasks = $final_tasks->where($search['type'], 'LIKE' , "%" . $search['query']. "%");
        }
        $final_tasks = $final_tasks->paginate(20);
        $pagination = $final_tasks->render();
        $pagination = str_replace("/?", "?", $pagination);
        return view('final_tasks.index', compact('search', 'final_tasks', 'pagination'));
    }

    public function create()
    {
        $final_task = new FinalTask();
        $department_options = Department::options();
        $score_options = FinalTask::scoreOptions();
        return view('final_tasks.create', compact('final_task', 'department_options', 'score_options'));
    }

    public function store(CreateFinalTaskRequest $request)
    {
        $final_task = new FinalTask($request->all());
        if (!$request->get('rate')) {
            $final_task->rate = null;
        }
        $final_task->save();
        return redirect()->route('root');
    }

    public function show($id)
    {
        $final_task = FinalTask::find($id);
        return $final_task;
    }

    public function edit($id)
    {
        $final_task = FinalTask::find($id);
        $department_options = Department::options();
        $score_options = FinalTask::scoreOptions();
        return view('final_tasks.edit', compact('final_task', 'department_options', 'score_options'));
    }


    public function update(UpdateFinalTaskRequest $request, $id)
    {
        $final_task = FinalTask::find($id);
        $final_task->update($request->all());
        if (!$request->get('rate')) {
            $final_task->rate = null;
            $final_task->save();
        }
        return redirect()->route('root');
    }

    public function destroy($id)
    {
        $final_task = FinalTask::find($id);
        $final_task->delete();
        return redirect()->route('root');
    }

    public function uploadCsv(Request $request)
    {
        return view('final_tasks.upload_csv');
    }

    public function storeCsv(Request $request)
    {
        $file = $request->file('csv_file');
        $filename = FinalTask::storeCsv($file);
        return redirect()->route('root');
    }
}
