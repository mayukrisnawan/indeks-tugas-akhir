<?php

Route::get('/', ['as' => 'root', 'uses' => 'FinalTasksController@index']);
Route::resource('final_tasks', 'FinalTasksController');

Route::get('/upload/csv', ['as' => 'upload_csv', 'uses' => 'FinalTasksController@uploadCsv']);
Route::post('/upload/csv', ['as' => 'store_csv', 'uses' => 'FinalTasksController@storeCsv']);