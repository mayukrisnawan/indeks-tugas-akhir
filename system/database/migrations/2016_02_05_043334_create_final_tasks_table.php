<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinalTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('final_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('student_name');
            $table->string('nim');
            $table->tinyInteger('department_id');
            $table->date('date')->nullable();
            $table->tinyInteger('rate')->nullable();
            $table->string('score')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('final_tasks');
    }
}
