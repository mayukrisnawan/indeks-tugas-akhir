$(document).ready(function(){
  $('body').on('click', '.confirmation', function(){
    var msg = $(this).attr('msg');
    var result = confirm(msg);
    if (!result) return false;
  });
});