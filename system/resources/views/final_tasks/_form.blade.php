{!! Form::hidden('id', null) !!}
<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
  <div class='col-sm-12'>
    {!! Form::label('title', 'Judul Tugas Akhir') !!}
    {!! Form::text('title', null,  ['class' => 'form-control']) !!}
    {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('nim') ? 'has-error' : '' }}">
  <div class='col-sm-2'>
    {!! Form::label('nim', 'NIM') !!}
    {!! Form::text('nim', null,  ['class' => 'form-control']) !!}
    {!! $errors->first('nim', '<span class="help-block">:message</span>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('student_name') ? 'has-error' : '' }}">
  <div class='col-sm-6'>
    {!! Form::label('student_name', 'Nama Mahasiswa') !!}
    {!! Form::text('student_name', null,  ['class' => 'form-control']) !!}
    {!! $errors->first('student_name', '<span class="help-block">:message</span>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('department_id') ? 'has-error' : '' }}">
  <div class='col-sm-2'>
    {!! Form::label('department_id', 'Jurusan') !!}
    {!! Form::select('department_id', $department_options, null, ['class' => 'form-control']) !!}
    {!! $errors->first('department_id', '<span class="help-block">:message</span>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
  <div class='col-sm-2'>
    {!! Form::label('date', 'Tanggal') !!}
    {!! Form::text(null, null, ['class'=>'form-control', 'id' => 'date', 'readonly'=>'true']) !!}
    {!! Form::hidden('date', $final_task->date, ['id' => 'date_hidden']) !!}
    {!! $errors->first('date', '<span class="help-block">:message</span>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('rate') ? 'has-error' : '' }}">
  <div class='col-sm-2'>
    {!! Form::label('rate', 'Rate') !!}
    {!! Form::number('rate', null,  ['class' => 'form-control']) !!}
    {!! $errors->first('rate', '<span class="help-block">:message</span>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('score') ? 'has-error' : '' }}">
  <div class='col-sm-2'>
    {!! Form::label('score', 'Nilai') !!}
    {!! Form::select('score', $score_options, null,  ['class' => 'form-control']) !!}
    {!! $errors->first('score', '<span class="help-block">:message</span>') !!}
  </div>
</div>

<div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
  <div class='col-sm-6'>
    {!! Form::label('comment', 'Komentar') !!}
    {!! Form::textarea('comment', null,  ['class' => 'form-control']) !!}
    {!! $errors->first('comment', '<span class="help-block">:message</span>') !!}
  </div>
</div>

<div class="form-group">
  <div class='col-sm-3'>
    {!! Form::Submit('Simpan', ['class' => 'btn btn-primary btn-save']) !!}
  </div>
</div>

<script type="text/javascript">
$("#date").datepicker({
  format: 'dd MM yyyy',
  language: 'id',
  autoclose: true
})
.on('changeDate', function(){
  var dt = $("#date").datepicker('getUTCDate');
  var month = dt.getUTCMonth()+1 < 10 ? "0" + (dt.getUTCMonth()+1) : dt.getUTCMonth()+1;
  var date = dt.getUTCDate() < 10 ? "0" + dt.getUTCDate() : dt.getUTCDate();
  dt = dt.getUTCFullYear() + "-" + month + "-" + date + " 00:00:00";
  $("#date_hidden").val(dt);
});
</script>