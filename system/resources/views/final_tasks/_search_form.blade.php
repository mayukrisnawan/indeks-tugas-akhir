{!! Form::open(['method' => 'GET', 'route' => ['root'], 'class' => 'form-inline', 'id' => 'search_form']) !!}
  <div class="form-group">
    {!! Form::select('type', [
      'nim' => 'NIM',
      'student_name' => 'Nama Mahasiswa',
      'title' => 'Judul Tugas Akhir'
    ], $search['type'] ? $search['type'] : 'name', ['class'=>'form-control', 'id' => 'search_type']); !!}
  </div>
  <div class="form-group">
    <div class="input-group">
      {!! Form::text('query', $search['query'] ? $search['query'] : null,  ['id' => 'search_query', 'class' => 'form-control input-large', 'size' => 50]) !!}
      <span class="input-group-btn">
        {!! Form::button('<i class="glyphicon glyphicon-search"></i>', ['class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'search_button']) !!}
      </span>
    </div>
  </div>
{!! Form::close() !!}