@extends('layouts.guest')

@section('content')
  <div class='row'>
    <div class='col-md-12'>
      <h1>Upload Daftar Tugas Akhir (.csv)</h1>
      <hr>
      {!! Form::open(['url' => route('store_csv'), 'files' => true]) !!}
        <div class='form-group'>
          {!! Form::file('csv_file', ['accept'=>'.csv']) !!}
        </div>
        <div class='form-group'>
          {!! Form::submit('Kirim', ['class'=>'btn btn-primary']) !!}
        </div>
      {!! Form::close() !!}
    </div>
  </div>
@stop