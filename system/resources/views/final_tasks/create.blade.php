@extends('layouts.guest')

@section('content')
<h1>Tambah Data Tugas Akhir</h1>
<hr>
{!! Form::open(['route'=>'final_tasks.store', 'class' => 'form-horizontal', 'role' => 'form']) !!}
  @include('final_tasks._form')
{!! Form::close() !!}

<script type="text/javascript">
var date = new Date();
$("#date").datepicker('setDate', date);
</script>
@stop