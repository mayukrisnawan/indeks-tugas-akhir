@extends('layouts.guest')

@section('content')
<h1>Edit Tugas Akhir</h1>
<hr>
{!! Form::model($final_task, ['method' => 'PATCH', 'route'=>['final_tasks.update', $final_task], 'class' => 'form-horizontal', 'role' => 'form']) !!}
  @include('final_tasks._form')
{!! Form::close() !!}
<script type="text/javascript">
var date = new Date({{ carbon_date($final_task->date)->timestamp * 1000 }});
$("#date").datepicker('setDate', date);
</script>
@stop