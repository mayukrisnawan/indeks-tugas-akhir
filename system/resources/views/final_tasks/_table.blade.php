<table class="table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th width="1%">#</th>
      <th>Judul Tugas Akhir</th>
      <th>NIM</th>
      <th>Nama Mahasiswa</th>
      <th>Jurusan</th>
      <th>Tanggal</th>
      <th>Rate</th>
      <th>Nilai</th>
      <th width="1%"></th>
    </tr>
  </thead>
  <tbody>
  @if(count($final_tasks) == 0)
    <tr>
      <td colspan="9" align='center'>
        <i>Belum ada tugas akhir yang terdaftar.</i>
      </td>
    </tr>
  @endif

  @foreach($final_tasks as $index => $final_task)
    <tr>
      <td>{{ $index+1 + $final_tasks->perPage() * ($final_tasks->currentPage()-1) }}</td>
      <td>{{ $final_task->title }}</td>
      <td>{{ $final_task->nim }}</td>
      <td>{{ $final_task->student_name }}</td>
      <td>{{ $final_task->department? $final_task->department->name : '-' }}</td>
      <td>{{ indo_date($final_task->date) }}</td>
      <td>{{ $final_task->rate }}</td>
      <td>{{ $final_task->score }}</td>
      <td>
        {!! Form::open(['method' => 'DELETE', 'route' => ['final_tasks.destroy', $final_task], 'style' => 'display:inline;']) !!}
          {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm confirmation', 'msg' => 'Apakah anda yakin ingin menghapus data berikut?', 'title' => 'Hapus']) !!}
        {!! Form::close() !!}
        <a href="{{ route('final_tasks.edit', $final_task) }}" class='btn btn-primary btn-sm' title='Edit'>
          <i class='glyphicon glyphicon-edit'></i>
        </a>
        @if ($final_task->comment)
          <a class='btn btn-info btn-sm btn-detail' title='Lihat Komentar' id='{{ $final_task->id }}'>
            <i class='glyphicon glyphicon-comment'></i>
          </a>
        @endif
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
{!! $pagination !!}

<div class="modal fade" id='modal_detail' role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Tutup"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Komentar Tugas Akhir</h3>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
      </div>
    </div>
  </div
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('body').on('click', '.btn-detail', function(){
      var id = $(this).attr('id');
      $.ajax({
        url:'{{ route('final_tasks.show', null) }}/' + id,
        dataType: 'json',
        success: function(response){
          $('#modal_detail .modal-body').html(response.comment);
          $('#modal_detail').modal('show');
        }
      });
    });
  });
</script>