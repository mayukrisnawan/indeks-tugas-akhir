@extends('layouts.guest')

@section('content')
  <div class='row'>
    <div class='col-md-12'>
      @include('final_tasks._search_form')
      <br>
      <a href="{{ route('upload_csv') }}" class='btn btn-primary'>
        <i class='glyphicon glyphicon-upload'></i>
        Upload (.csv)
      </a>
      <a href="{{ route('final_tasks.create') }}" class='btn btn-primary'>
        <i class='glyphicon glyphicon-plus'></i>
        Tambah Data
      </a>
      <hr>
      <div class='table-responsive'>
        @include('final_tasks._table')
      </div>
    </div>
  </div>
@stop