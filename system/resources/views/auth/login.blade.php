@extends('layouts.guest')

@section('content')
<div style="width: 400px; margin: 80px auto; border: solid 1px #eee; padding: 50px; text-align: center">
  <h3>Login</h3>
  <hr>
  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  <form role="form" name="user_form" class="form-horizontal" method="POST" action="{{ action('Auth\AuthController@getLogin') }}">
    <div class="flash alert" style="display:none"></div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
      <input type="text" name="username" class="form-control" placeholder="Username">
    </div>
    <div class="form-group">
      <input type="password" name="password" class="form-control" placeholder="Password">
    </div>
    <div class="form-group">
      <input type="submit" class="btn btn-default" value="Sign in">
    </div>
  </form>
</div>
@stop