@extends('layouts.guest')

@section('content')
<div style="width: 400px; margin: 80px auto; border: solid 1px #eee; padding: 50px; text-align: center">
  @foreach ($errors->all() as $error)
  <div>
      {{ $error }}
  </div>
  @endforeach 
  <h3>Register</h3>
  <hr>
  <form role="form" name="user_form" class="form-horizontal" method="POST" action="/auth/register">
    <div class="flash alert" style="display:none"></div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
      <input type="text" name="name" class="form-control" placeholder="Username">
    </div>
    <div class="form-group">
      <input type="text" name="email" class="form-control" placeholder="Email">
    </div>
    <div class="form-group">
      <input type="password" name="password" class="form-control" placeholder="Password">
    </div>
        <div class="form-group">
      <input type="password" name="password_confirmation" class="form-control" placeholder="Password Confirmation">
    </div>
    <div class="form-group">
      <input type="submit" class="btn btn-default" value="Log in">
    </div>
  </form>
</div>
@stop