<!DOCTYPE html>
<html>
<head>
  <title>Indeks Tugas Akhir</title>
  <link rel="stylesheet" type="text/css" href="{{ custom_elixir('css/app.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('build/css/font-awesome/font-awesome.min.css') }}">
  <script type="text/javascript" src="{{ custom_elixir('js/app.js') }}"></script>
</head>
<body>
  <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Tampilkan/Sembunyikan Menu</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="{{ route('root') }}" class="navbar-brand">INDEKS TUGAS AKHIR</a>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        @yield('content')
      </div>
    </div>
  </div>
</body>
</html>