var elixir = require('laravel-elixir');
elixir.config.sourcemaps = false;
elixir.config.publicPath = '../';

elixir(function(mix) {
    mix.less("app.less", "../css/app.css")
    mix.scripts([
      '../../../node_modules/jquery/dist/jquery.js',
      '../../../node_modules/bootstrap-less/js/bootstrap.js',
      '../../../node_modules/bootstrap-datepicker/js/bootstrap-datepicker.js',
      '../../../node_modules/bootstrap-datepicker/js/locales/bootstrap-datepicker.id.js',
      'ui.js'
    ],'../js/app.js');
    mix.version(['../css/app.css', '../js/app.js']);
});