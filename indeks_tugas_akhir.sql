-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2016 at 10:25 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `indeks_tugas_akhir`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'MTI', '2016-02-04 23:16:21', '2016-02-04 23:16:21'),
(2, 'DGM', '2016-02-04 23:16:22', '2016-02-04 23:16:22'),
(3, 'SK', '2016-02-04 23:16:22', '2016-02-04 23:16:22');

-- --------------------------------------------------------

--
-- Table structure for table `final_tasks`
--

CREATE TABLE IF NOT EXISTS `final_tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `student_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_id` tinyint(4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `rate` tinyint(4) DEFAULT NULL,
  `score` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=109 ;

--
-- Dumping data for table `final_tasks`
--

INSERT INTO `final_tasks` (`id`, `title`, `student_name`, `nim`, `department_id`, `date`, `rate`, `score`, `comment`, `created_at`, `updated_at`) VALUES
(1, 'SISTEM PAKAR MENDIAGNOSA PENYAKIT PADA BABI DENGAN MENGGUNAKAN METODE DEMOSTER SHAFER BERBASIS ANDROID', 'I MADE TEGUH DARMADI WIRAHARJA', '11101052', 1, '2015-08-26', 4, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(2, 'RANCANG BANGUN DATA WAREHOUSE PADA RUMAH SAKIT UMUM DAERAH WANGAYA KOTA DENPASAR', 'KOMANG MAHARINI', '12101333', 1, '2015-09-01', 6, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(3, 'SISTEM INFORMASI DIGITAL LIBRARY TUGAS AKHIR STMIK STIKOM INDONESIA', 'ROVICKY VENDY DAHLAN', '11101254', 1, '2015-09-02', 5, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(4, 'RANCANG BANGUN SISTEM PAKAR MENDIAGNOSA GANGGUAN PADA MASA KEHAMILAN MENGGUNAKAN METODE DEMPSTER SHAFER', 'NI PUTU TINA JULIANTI', '11101081', 1, '2015-09-01', 8, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(5, 'SISTEM INFORMASI SURVEY PELANGGAN TEGANGAN RENDAH PADA PT PLN (PERSERO) AREA BALI SELATAN DENGAN VISUALISASI GEOGRAFI ONLINE', 'I PUTU NIKAYANA', '11101161', 1, '2015-08-31', 6, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(6, 'RANCANG BANGUN E-COMMERCE ART SHOP JENY29 ONE STOP SHOP', 'I KADEK ARY ENDRA WIGUNA', '11101378', 1, '2015-09-01', 3, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(7, 'RANCANG BANGUN SISTEM KNOWLEDGE SHARING BERBASIS MOBILE DAN WEB PADA STMIK STIKOM INDONESIA', 'I GEDE BARA YUDA GAUTAMA', '11101427', 1, '2015-09-04', 12, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(8, 'RANCANG BANGUN SISTEM PENDUKUNG KEPUTUSAN PENERIMAAN CALON SISWA BARU DI SMK BABUS SALAM DENGAN METODE PROMETHEE BERBASIS WEB', 'RISKIA FATIA WIRDA', '11101045', 1, '2015-09-02', 3, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(9, 'RANCANG BANGUN SISTEM PAKAR MENDIAGNOSA PENYAKIT PADA ANJING DENGAN MENGGUNAKAN TEORAMA BAYES BERBASIS WEB', 'ANDRE AVARELLI HUTABARAT', '11101097', 1, '2015-09-03', 4, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(10, 'SISTEM INFORMASI AKADEMIK SD NOMOR 1 KUTA BERBASIS WEB', 'NYOMAN TRI SUTRISNA', '11101102', 1, '2015-09-02', 10, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(11, 'RANCANG BANGUN SISTEM DIAGNOSA PENYAKIT PADA TERNAK KAMBING DENGAN MENGGUNAKAN METODE DEMPSTER-SHAFER BERBASIS WEB', 'I KOMANG WIJAYA', '11101422', 1, '2015-09-01', 3, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(12, 'RANCANG BANGUN SISTEM INFORMASI RESERVASI THE LIGHT EXCLUSIVE VILLA & SPA BERBASIS WEB', 'RYU SUGIARTO', '11101012', 1, '2015-09-01', 5, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(13, 'APLIKASI PENJUALAN BERBASIS WEB E-COMMERCE STUDI KASUS PADA AGUNG TEKNIK POMPA', 'NI LUH PUTU PASEK RISKA DEWI', '11101110', 1, '2015-08-31', 3, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(14, 'SISTEM INFORMASI AKADEMIK DENGAN KURIKULUM 2013 BERBASIS WEB PADA SMA KRISTEN HARAPAN DENPASAR', 'I WAYAN SUMARDIANA', '11101008', 1, '2015-08-31', 3, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(15, 'SISTEM PAKAR DIAGNOSA PENYAKIT SAPI MENGGUNAKAN METODE DEMPSTER SHAVER BERBASIS WEB', 'I WAYAN ANDRE SUTIRTA YASA', '11101250', 1, '2015-09-02', 3, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(16, 'SISTEM INFORMASI GEOGRAFI LETAK PENYEDIA LAYANAN VCT (VOLUNTARY COUNSELING AND'' TESTING) HIV AIDS DI DENPASAR BERBASIS ANDROID', 'M. RUDI GUNAWAN PAROZAK', '10101120', 1, '2015-08-31', 5, 'A', 'layak tapi sangat perlu perbaikan pada bagian aplikasi', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(17, 'SISTEM INFORMASI AKADEMIK SERTA PENENTUAN KELAS UNGGULAN MENGGUNAKAN METODE CLUSTTERING DENGAN ALGORITMA K-MEANS DI SMP NEGERI 3 UBUD', 'I KADEK ARIK SUANTARA', '11101082', 1, '2015-09-02', 5, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(18, 'SISTEM INFORMASI PENCAPAIAN PERINDUSTRIAN DAN PEMASANGAN SAMPEL PADA PT. PLATINUM CERAMICS INDUSTRI', 'MAWAR INDAH AGUSTINI', '11101147', 1, '2015-09-02', 5, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(19, 'SISTEM PENDUKUNG KEPUTUSAN PEMBERIAN KREDIT PADA NASABAH DI LPD DESA ADAT KEROBOKAN MENGGUNKAN METODE ANALYTICAL HIERARCHY PROCESS AHP', 'IDA BAGUS GEDE WEDA ADNYANA', '11101144', 1, '2015-09-03', 4, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(20, 'RANCANG BANGUN APLIKASI SISTEM INFORMASI ADMINISTRASI SEKOLAH BERBASIS WEB (STUDI KASUS SMK MUHAMMADIYAH DENPASAR) ', 'MUHAMMAD FHAUZAN', '11101245', 1, '2015-09-04', 6, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(21, 'RANCANG BANGUN E-COMMERCE PADA CV. TRI BINTANG JAYA', 'I WAYAN YUDIANA', '11101260', 1, '2015-09-03', 4, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(22, 'RANCANG BANGUN SISTEM INFORMASI MANAJEMEN INVENTORY PADA UD. SINAR SEJAHTERA', 'NI NYOMAN SUTRISNAWATI', '11101112', 1, '2015-09-04', 4, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(23, 'RANCANG BANGUN E-COMMERCE LUKISAN (STUDI KASUS PADA NARAYANA PAINTING)', 'NI WAYAN RATNA RIARTINI', '11101419', 1, '2015-09-04', 6, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(24, 'RANCANG BANGUN SISTEM PAKAR BERBASIS WEB UNTUK MEMBANTU MENDIAGNOSA AWAL PENYAKIT AKIBAT PERANTARA NYAMUK DENGAN MENGGUNAKAN METODE DEMPSTER SHAFER', 'I PUTU BALI PRADIKA', '11101277', 1, '2015-08-31', 5, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(25, 'RANCANG BANGUN SISTEM E-CURRENCY PADA PT. SUN INVESTAMA', 'NI KADEK ENI PUSPAYANTI', '11101137', 1, '2015-09-01', 4, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(26, 'SISTEM PENDUKUNG KEPUTUSAN PEMBERIAN REKOMENDASI BANTUAN PINJAMAN DANA PADA UKM MENGGUNAKAN METODE AHP DI KABUPATEN GIANYAR', 'NI WAYAN ANIK WIDIARI', '11101127', 1, '2015-09-02', 3, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(27, 'PERAMALAN CALON MAHASISWA BARU STMIK STIKOM INDONESIA MENGGUNAKAN METODE SINGLE MOVING AVERAGE', 'ARI RUDIYAN', '12101068', 1, '2015-09-02', 3, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(28, 'SISTEM PENDUKUNG KEPUTUSAN KENAIKAN JABATAN PADA LEMBAGA PERKREDITAN DESA ADAT TEGAL DENGAN METODE PROFILE MATCHING ', 'SI LUH OKA PRIDAYANTI', '11101169', 1, '2015-08-28', NULL, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(29, 'RANCANG BANGUN APLIKASI PEMBELAJARAN KESENIAN BALI BERBASIS WEB DENGAN ALGORITMA FISHER YATES SHUFFLE', 'NI WAYAN JERI KUSUMA DEWI', '11101294', 1, '2015-05-30', 4, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(30, 'RANCANG BANGUN SISTEM INFORMASI INVENTORY DAN PERAMALAN PENJUALAN DENGAN METODE SINGLE MOVING AVERAGE PADA TOKO RUMAH HELM', 'ARIE PANGESTU', '11101115', 1, '2015-06-03', 5, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(31, 'SISTEM MONITORING PEMBIMBING KERJA PRAKTEK DAN TUGAS AKHIR PADA STIMIK STIKOM INDONESIA BERBASIS WEB', 'I NENGAH ARYA SANTIKA', '11101326', 1, '2015-08-31', 6, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(32, 'RANCANG BANGUN SISTEM OPTIMASI PENGGUNAAN BAHAN BAKU UNTUK MEMAKSIMALKAN KEUNTUNGAN DENGAN METODE SIMPLEKS PADA PERUSAHAAN REPUBLIC OF SOAP', 'KADEK ANGGIE PRASETYA HOGANTARA', '11101287', 1, '2015-05-30', 4, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(33, 'RANCANG BANGUN E-COMMERC SERULING DEWATA SHOP STUDI KASUS PESRAMAN SERULING DEWATA', 'I WAYAN PRIMA ARISTHA', '11101039', 1, '2015-08-28', 4, 'A', '', '2016-02-05 01:23:02', '2016-02-05 01:23:02'),
(34, 'RANCANG BANGUN SISTEM INFORMASI INVENTORI BERBASIS DEKSTOP PADA MINI MARKET SURYA MART', 'NI LUH PUTU OKTAVIANI', '11101218', 1, '2015-08-28', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(35, 'RANCANG BANGUN PERAMALAN TINGKAT KUNJUNGAN WISATAWAN DENGAN METODE AVERAGE BASED FUZZY TIME SERIES DAN MARKOV CHAIN MODEL DI SRIPHALA RESORT & VILLA', 'I GEDE SEDANA ARTA', '11101196', 1, '2015-08-29', 4, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(36, 'RANCANG BANGUN SISTEM DETEKSI PLAGIAT DOKUMEN BERBAHASA INDONESIA MENGGUNAKAN METODE LEVENSHTEIN DISTANCE DAN SYNONYM RECOGNITION', 'NI MADE AYU PUSPITA ANGGARAYANI', '11101389', 1, '2015-05-29', 4, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(37, 'APLIKASI PENGINGAT WAKTU SHALAT DAN PENENTU ARAH KIBLAT MENGGUNAKAN GOBAL POSITIONING SISTEM (GPS) BERBASIS ANDROID', 'HANDRIAN SAPUTRA', '11101096', 1, '2015-06-09', 4, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(38, 'RANCANG BANGUN SISTEM INFORMASI MANUFAKTUR PADA UD. CETROENG', 'PUTU LILIES WIRANTI', '11101204', 1, '2015-08-28', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(39, 'SISTEM INFORMASI PENCARIAN RUMAH KOS DI DAERAH DENPASAR MENGGUNAKAN METODE WEIGHTED PRODUCT', 'I MADE DANUARTHA', '11101190', 1, '2015-09-01', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(40, 'SISTEM PENDUKUNG KEPUTUSAN PENERIMAAN KARYAWAN BARU MENGGUNAKAN METODE ANALYTICAL HIERARCHI PROCESS (AHP) PADA PT. BIOMETRIK CITRA SOLUSI CABANG DENPASAR', 'NI WAYAN SULASMINI', '11101066', 1, '2015-09-04', 4, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(41, 'APLIKASI PENCARIAN LETAK PURA DI LOMBOK MENGGUNAKAN HAVERSINE FORMULA BERBASIS ANDROID', 'INDRA PRATISTHA', '11101375', 1, '2015-09-02', NULL, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(42, 'SISTEM INFORMASI OPTIMASI PENDISTRIBUSIAN KELAS PADA DOSEN DI STMIK STIKOM INDONESIA MENGGUNAKAN ALGORITMA GENETIKA', 'I GEDE OKA PALGUNA', '11101392', 1, '2015-09-04', 5, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(43, 'ANALISIS KERANJANG BELANJA PADA PERUSAHAAN RETAIL DENGAN MENGGUNAKAN METODE ALGORITMA APRIORI DAN PROFSET MODEL', 'PUTRI AGUNG PERMATASARI', '12101053', 1, '2015-09-04', 9, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(44, 'RANCANG BANGUN SISTEM INFORMASI INVENTORY PADA UD. MIASA BERBASIS DEKSTOP', 'I PUTU ARYA DIANA', '11101143', 1, '2015-09-01', 5, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(45, 'PERANCANGAN DAN PEMBANGUNAN VIDEO IKLAN LAYANAN MASYARAKAT TENTANG PENCEGAHAN DAN PENANGGULANGAN KEBAKARAN BERBASIS ANIMASI 2 DIMENSI', 'ANGGA FEBDY VIANTO', '10101181', 2, '2015-08-31', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(46, 'RANCANG BANGUN VIDEO ANIMASI PENGENALAN LAYANAN KOMPREHENSIF HIV/AIDS BERKESINAMBUNGAN BERBASIS ANIMASI 2D', 'I PUTU ARYA WIDYANATA', '11101063', 2, '2015-08-31', 6, 'A', 'pak githa: sedikit dipersingkat durasi; pak made: aset visual animasi bisa diimprovisasi agar tidak terlalu banyak perulangan', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(47, 'PERANCANGAN VIDEO COMPANY PROFILE SEBAGAI MEDIA PENDUKUNG PROMOSI CV. NYOMAN GRAFIS DESIGN', 'I KETUT SUPARWATA', '11101009', 2, '2015-08-31', 4, 'A', 'pak made: video company profile sebagai hasil penelitian terkait dengan perusahaan nyoman grafis design, haki akan berhubungan dengan kepemilikan video nantinya. Perlu dipertimbangkan hal ini baik-baik', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(48, 'RANCANG BANGUN VIDEO ANIMASI MANFAAT HUTAN MANGROVE BAGI LINGKUNGAN BERBASIS ANIMASI 2D', 'I KADEK SURYAWAN', '11101046', 2, '2015-08-31', 10, 'A', 'Pak Githa: dengan perbaikan suara dan musik; Pak Made: Narasi diganti suara wanita, perhatikan intonasi dan kejernihan suara, tambahkan logo stiki, ganti musik latar', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(49, 'PERANCANGAN VIDEO EDUKASI TENTANG BAHAYA ABORSI DI KALANGAN REMAJA BERBASIS ANIMASI 2 DIMENSI', 'INDHESWARI GITA SHANTI', '11101320', 2, '2015-09-02', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(50, 'RANCANG ULANG LAYOUT WEBSITE HOTEL RAMADA ENCORE BALI SEMINYAK BERBASIS WEB RESPONSIVE', 'I KOMANG KRIS SURIAWAN', '11101021', 2, '2015-09-02', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(51, 'RANCANG BANGUN WEBSITE PROMOSI PADA GRIYA TANTRI CONTRACTOR & PROPERTY', 'I WAYAN SUARDIANA PUTRA', '11101056', 2, '2015-09-02', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(52, 'IMPLEMENTASI MARKERLESS AUGMENTED REALITY PADA MEDIA INFORMASI INTERAKTIF MR. KUTA BALI', 'RIZKY BHARA PRAKOSO', '11101189', 2, '2015-09-01', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(53, 'PERANCANGAN VIDEO PROFIL IKATAN PELAJAR MUHAMMADIYAH KABUPATEN BANYUWANGI BERBASIS ANIMASI 2 DIMENSI', 'EDI SANTOSO', '9101364', 2, '2015-07-09', 3, 'A', 'Bu Aniek: karena video yang ditampilkan banyak teks dan gambar yang tidak bergerak; Pak Anom: karena sifatnya mengkhusus dan terkait dengan organisasi sebagai asas pertimbangan terakhir kurang layak untuk haki; bisa ada ketidakpastian dalam kepemilikan / royalti animasi karena animasi akan diserahkan kepada ormas ipm', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(54, 'PERANCANGAN MEDIA PEMBELAJARAN PENGENALAN DAN PENGHITUNGAN LUAS DAN KELILING BANGUN DATAR BERBASIS ANIMASI 2D', 'I PUTU HERI EKA PUTRA', '11101141', 2, '2015-09-04', 4, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(55, 'PERANCANGAN MEDIA PEMBELAJARAN ALAT MUSIK TRADISIONAL CUNGKLIK BERBASIS MULTIMEDIA INTERAKTIF', 'KADEK RUSPIKAYANA', '10101143', 2, '2015-06-01', 6, 'A', 'Bu Aniek: ulang ngerekam, kesalahan dalam pembelajaran, perbaikan tangga nada, resolusi; Pak Jaya: konsep dan ide dari multimedia interaktif cukup menarik namun tentunya diperlukan pengembangan lebih lanjut agar multimedia lebih menarik dan tentunya bermanfaat bagi masyarakat', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(56, 'PERANCANGAN IKLAN LAYANAN MASYARAKAT PENGENALAN DAN PENCEGAHAN PENYAKIT OSTEOPOROSIS BERBASIS ANIMASI MOTION GRAPHIC', 'I MADE SUARTAWAN', '11101016', 2, '2015-08-31', 6, 'A', 'Pak githa: layak tetapi harus diperbaiki dan diperhalus animasinya', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(57, 'PERANCANGAN MEDIA PEMBELAJARAN PENGENALAN SIFAT-SIFAT BENDA BERBASIS MULTIMEDIA INTERAKTIF (STUDI KASUS : SDN 24 DAUH PURI)', 'IKA AMELLIA RIZANTY', '11101252', 2, '2015-09-03', 8, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(58, 'RANCANG BANGUN ANIMASI 2D PENGENALAN SISTEM BANK SAMPAH PADA ANAK', 'I PUTU HENDRA NUGRAHA', '11101426', 2, '2015-09-02', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(59, 'RANCANG BANGUN MEDIA PROMOSI IKLAN JASA TRAVEL BERBASIS ANIMASI 2D (OBJEK KASUS : GRIYA TRANS WISATA)', 'DANIEL ANDRA', '11101075', 2, '2015-09-02', NULL, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(60, 'IMPLEMENTASI AUGMENTED REALITY PADA BROSUR PRODUK MOBIL HONDA BERBASIS ANDROID', 'INDRA RISKY NURADI SASONGKO', '11101396', 2, '2015-09-02', 7, 'A', 'pak anom: mengandung produk resmi', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(61, 'PERANCANGAN VIDIO IKLAN LAYANAN MASYARAKAT TENTANG BANK SAMPAH PADA KOTA DENPASAR BERBASIS ANIMASI 2 DIMENSI', 'I PUTU ARI KURNIAWAN', '11101049', 2, '2015-08-31', 6, 'A', 'pak made: perlu dipilah hak cipta oleh mahasiswa dan stiki, dan hak guna atau hak milik apakah berada pada stiki atau dkp kota denpasar', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(62, 'PERANCANGAN IKLAN LAYANAN MASYARAKAT "TERTIB LALU LINTAS" BERBASIS ANIMASI 2D', 'KELIK DILIANTO', '10101395', 2, '2015-06-11', 6, 'A', 'Pak Made: pendaftaran hak cipta untuk karya animasi lebih memungkinkan dibandingkan untu kkarya videografi khususnya live action karena karya videografi live action melibatkan orang yang harus memberikan konfirmasi atau pernyataan mengenai orisinalitas video; Pak Dewa Adi: Masukkan logo stiki; perlu dilakukan perbaikan atau penyempurnaan video agar pesan yang disampaikan lebih jelas', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(63, 'PERANCANGAN INTRAKTIF MULTIMEDIA UNTUK PEMBELAJARAN MATEMATIKA KELAS 2 TINGKAT SEKOLAH DASR DI BALI KIDDY', 'DYAH AYU SUARINI', '11101184', 2, '2015-05-30', 5, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(64, 'PERANCANGAN VIDIO ANIMASI 2D TENTANG LANGKAH-LANGKAH PEMBUATAN SERTIFIKAT KESEHATAN IKAN LAYAK KONSUMSI', 'MUHAMMAD KHOLIL', '11101251', 2, '2015-09-04', 4, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(65, 'PERANCANGAN VIDIO ANIMASI 2D TENTANG SAFETY INDUCTION PADA TERMINAL BBM PT. PERTAMINA (PERSERO) SANGGARAN', 'MUHAMMAD EFFENDY', '11101084', 2, '2015-09-04', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(66, 'PERANCANGAN VIDIO ANIMASI 2D PENGENALAN BAHAYA MERKURI TERHADAP MASYARAKAT KABUPATEN LOMBOK TENGAH SEBAGAI DAMPAK PENAMBANGAN EMAS ILEGAL', 'LAKSMINTA RUKMI', '11101316', 2, '2015-09-04', NULL, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(67, 'RANCANG BANGUN MULTIMEDIA INTERAKTIF PEMBELAJARAN SISTEM BASIS DATA DI STMIK STIKOM INDONESIA', 'PUTU YUDIK JUNIARTA', '11101404', 2, '2015-09-04', 5, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(68, 'PERANCANGAN MEDIA INTERAKTIF PENGENALAN TARIAN TRADISIONAL TIMOR LESTE BERBASIS ANDROID', 'GIONELTO CASTEN DOLFRI DO ROSARIO TEIXEIRA', '10101345', 2, '2015-09-03', 5, 'A', 'sistem dan informasinya kurang lengkap; pak githa: layak dengan perbaikan dan penyempurnaan aplikasi', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(69, 'PERANCANGAN VIDIO IKLAN LAYANAN MASYARAKAT TENTANG CARA SEHAT MENONTON TV UNTUK ANAK BERBASIS ANIMASI 2 DIMENSI', 'KAMALIYAH', '11101341', 2, '2015-09-04', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(70, 'VIDIO ANIMASI 2D TENTANG TATA CARA MENYUSUN KRS PADA KAMPUS STIKI INDONESIA', 'VITALIS VICKO DWI PUTRA', '11101289', 2, '2015-09-04', 8, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(71, 'VIDEO ANIMASI PENTINGNYA IMUNISASI BAGI BAYI USIA 0 SAMPAI 2 TAHUN BERBASIS ANIMASI 2D', 'NI KOMANG YULIANI', '11101037', 2, '2015-09-01', 5, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(72, 'INTERAKTIF MULTIMEDIA PEMBELAJARAN PENGENALAN AIR, BUMI DAN MATAHARI UNTUK KELAS 2 SD', 'IDA AYU WIDYASTITI', '11101388', 2, '2015-08-31', 5, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(73, 'PERANCANGAN MEDIA PEMBELAJARAN INTERAKTIF MATAKULIAH PRATIKUM JARINGAN KOMPUTER PADA STMIK STIKOM INDONESIA', 'NI LUH TATIK ARYANINGSIH', '11101173', 2, '2015-09-01', 5, 'A', 'pak made: project tugas akhir ini ditujukan untuk membantu proses perkuliahan di stiki, maka hak cipta bisa dimiliki stiki dengan menyertakan nama mahasiswa pembuat karya, tetapi perlu memperhatikan materi-materi pada hasil akhir ta yang memiliki kemungkinan kepemilikan haki orang/instansi lain, misalnya aset bagian musik', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(74, 'RANCANG BANGUN VIDEO ANIMASI BAHAYA OBESITAS BAGI KESEHATAN BERBASIS ANIMASI 2D', 'I PUTU AGUS SUARTAMA', '11101095', 2, '2015-08-31', 5, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(75, 'RANCANG BANGUN VIDEO ANIMASI 2D TENTANG PENTINGNYA MENGKONSUMSI GARAM BERYODIUM PADA MASYARAKAT', 'NI PUTU SWANDEWI KARTIKA SARI', '11101078', 2, '2015-08-31', 8, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(76, 'PERANCANGAN VIDIO ANIMASI PENCEGAHAN KAKI DIABETES PADA PASIEN DIABETES MELITUS BERBASIS ANIMASI 2D', 'NI KADEK EMI WIDIA SARI', '11101124', 2, '2015-08-31', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(77, 'PERANCANGAN MULTIMEDIA INTERAKTIF UNTUK PEMBELAJARAN MATA KULIAH ANIMASI 2D DI STIMIK STIKOM INDONESIA', 'I WAYAN EKA DWIPAYANA PUTRA', '11101191', 2, '2015-08-29', 6, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(78, 'VIDEO MAPPING BERTEMAKAN BAHAYA PENGGUNAAN HANDPHONE SAAT BERKENDARA', 'I GEDE TANGKAS ARIMBHAWA', '11101024', 2, '2015-08-26', 6, 'A', 'namun perlu video yang belum pernah ada', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(79, 'RANCANG BANGUN MEDIA PEMBELAJARAN PERKALIAN DAN PEMBAGIAN MENGGUNAKAN METODE JARIMATIKA BERBASIS INTERAKTIF MULTIMEDIA', 'NI WAYAN YUNI ANDI PRATINI', '11101133', 2, '2015-08-29', 5, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(80, 'RANCANG BANGUN MULTIMEDIA INTERAKTIF PEMBELAJARAN PADA MATA KULIAH PEMROGRAMAN 1 DI STIMIK STIKOM INDONESIA', 'GEDE KARIVANI ARNATA', '11101412', 2, '2015-08-29', 8, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(81, 'RANCANG BANGUN SISTEM GRIPPER PADA ROBOT REMOTELY UNDERWATER VEHICLE UNTUK PENGANGKATAN OBYEK BAWAH LAUT', 'MUHAMMAD TAUFIK', '11102083', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(82, 'RANCANG BANGUN SISTEM MEMBUKA DAN MENUTUP PINTU GERBANG BERBASIS SMS GATEWAY', 'RICARD CALVIN MISSA', '11102342', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(83, 'RANCANG BANGUN PENGENDALI LAMPU PENERANGAN JALAN BERBASIS ARDUINO DAN SENSOR PIR', 'I NYOMAN HARTANA', '11102090', 3, '2015-09-04', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(84, 'SISTEM NAVIGASI ARAH REMOTELY OPERATED VEHICLE MENGGUNAKAN SENSOR KOMPAS CMPS03', 'I MADE GEDE MAHA ADNYANA RL', '11102230', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(85, 'RANCANG BANGUN SISTEM PEMBERIAN MAKAN DAN MINUM HEWAN OTOMATIS BERBASIS SMS GATEWAY', 'I GUSTI NGURAH EKA PUTRA', '11102041', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(86, 'RANCANG BANGUN REMOTE OPERATED VEHICLE UNTUK MELAKUKAN PENCARIAN KAPAL KARAM', 'I WAYAN PUTRA SANJAYA', '11102323', 3, '2015-09-03', 3, 'A', 'fokus camera', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(87, 'RANCANG BANGUN SISTEM PENGISIAN SERBUK KAYU SEBAGAI BAHAN BAKAR SISTEM PENGERINGAN KAYU OLAHAN BERBASIS SMS GATEWAY', 'ZEFRY WAHYU SUCIADI', '11102383', 3, '2015-09-03', 3, 'A', 'pak agung: lebih bagus lage', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(88, 'MERANCANG DAN MEMBANGUN ATAP PENGATUR INTENSITAS CAHAYA MATAHARI MENGGUNAKAN SENSOR CAHAYA DAN SENSOR HUJAN PADA KOLAM RENANG', 'I WAYAN YOGI RADITYA', '11102122', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(89, 'RANCANG BANGUN REMOTELY OPERATED VEHICLE UNTUK PEMANTAUAN TERUMBU KARANG DENGAN KENEKSI RADIO', 'I NYOMAN YOHANES CHRISTIAN SAPUTRA', '11102089', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(90, 'RANCANG BANGUN ALAT PENYEMPROT PESTISIDA PORTABLE DENGAN KENDALI WIRELESS', 'I PUTU AGUNG SUMARTAWAN', '11102365', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(91, 'RANCANG BANGUN SISTEM WIPER OTOMATIS PADA KACA HELM BERBASIS ATMEGA328 UNTUK MENGHINDARI GANGGUAN PENGLIHATAN SAAT HUJAN', 'I PUTU HARY DHARMA SAPUTRA', '11102151', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(92, 'RANCANG BANGUN LED CUBE 8X8X8 SEBAGAI PENAMPIL ANIMASI 3D', 'I MADE YASA', '11102121', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(93, 'RANCANG BANGUN SISTEM PEMILIHAN BUAH JAMBU AIR BERDASARKAN WARNA MENGGUNAKAN SENSOR LDR', 'I GEDE ANDIKA', '11102340', 3, '2015-09-03', 4, 'A', 'Pak Arsa: belum untuk haki; Pak Gung Eka: dikembangkan lagi agar lebih presisi', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(94, 'RANCANG BANGUN LED CUBE 8X8X8 SEBAGAI PENAMPIL ANIMASI ANGKA', 'PUTU FERRY WIRATMAJA', '10102399', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(95, 'SISTEM PENENTUAN LOKASI HELM YANG HILANG MENGGUNAKAN GPS BERBASIS ATMEGA328', 'KADEK MAGSEGA ARTAWAN', '11102158', 3, '2015-09-02', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(96, 'RANCANG BANGUN PERANGKAP LANDAK OTOMATIS DENGAN FASILITAS SHORT MESSAGE SERVICE', 'I GEDE EKA DARMAWAN', '11102257', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(97, 'SISTEM PENGONTROL SUHU DAN SIRKULASI UDARA PADA KANDANG AYAM SECARA OTOMATIS', 'I PUTU AGUS EKA PUTRA', '11102286', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(98, 'RANCANG BANGUN SISTEM PENDETEKSI GEMPA BUMI DENGAN SENSOR GETARAN INDUKSI ELEKTROMAGNETIK BERBASIS ATMEGA328', 'I MADE ADI PUTRA', '11102366', 3, '2015-09-04', 12, 'A', 'pak gung eka: good; perlu kalibrasi alat untuk menguji kinerja', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(99, 'RANCANG BANGUN ALAT PENETAS TELUR AYAM OTOMATIS MENGGUNAKAN SENSOR SUHU DHT 11', 'I MADE YUDI WIJAYA PANDE', '12102427', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(100, 'RANCANG BANGUN ALAT PENYEMPROT CAT OTOMATIS UNTUK KERAJINAN PATUNG', 'I GUSTI NYOMAN NGURAH TRI ANTARA', '11102359', 3, '2015-09-03', 4, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(101, 'RANCANG BANGUN ARSITEKTUR QUADCOPTER BERBASIS FLIGHT CONTROLLER KK BOARD UNTUK MENJAGA KESTABILAN TERBANG DI UDARA', 'NI NYOMAN PUTRI DESIANI', '11102187', 3, '2015-09-04', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(102, 'RANCANG BANGUN QUADCOPTER OBSTACLE AVOIDANCE DENGAN SENSOR INFRAMERAH SEBAGAI SARANA PEMANTAUAN BENCANA ALAM MELALUI UDARA', 'MADE CARDEWA', '11102130', 3, '2015-09-02', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(103, 'RANCANG BANGUN LED CUBE 8X8X8 DENGAN ANIMASI HURUF 3D SEBAGAI MEDIA PEMBELAJARAN', 'I GEDE ARIESMAWAN PUSDIKA', '11102440', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(104, 'RANCANG BANGUN SISTEM PERGERAKAN DASAR QUADCOPTER BERBASIS KK BOARD SEBAGAI FLIGHT CONTROLLER', 'GEDE AGUS ARYA SABARI', '11102221', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(105, 'PERANCANGAN DAN IMPLEMENTASI QUADCOPTER DENGAN KAMERA NIGHT VISION SEBAGAI SARANA PEMANTAUAN DI MALAM HARI', 'GODLIEF MADI MARISSIE', '11102117', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(106, 'RANCANG BANGUN SISTEM PENYIRAMAN OTOMATIS TANAMAN ANGGREK PADA MODEL VERTICAL GARDEN MENGGUNAKAN SENSOR KELEMBABAN DAN SUHU', 'I GEDE YOGA ADI ARSANA', '11102198', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(107, 'RANCANG BANGUN ALAT PENGERING RUMPUT LAUT OTOMATIS', 'I KADEK WIDHI SANJAYA', '11102159', 3, '2015-09-03', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03'),
(108, 'RANCANG BANGUN REMOTELY OPERATED VEHICLE UNTUK OBSERVASI HEWAN LAUT MENGGUNAKAN WEB CAMERA', 'I GEDE SEPTAHADI PUTRA', '11102183', 3, '2015-09-04', 3, 'A', '', '2016-02-05 01:23:03', '2016-02-05 01:23:03');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_05_043334_create_final_tasks_table', 1),
('2016_02_05_044301_create_departments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
